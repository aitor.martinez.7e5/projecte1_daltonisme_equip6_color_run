using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SpawnPlatforms : MonoBehaviour
{
    [SerializeField] private GameObject[] _platform;
    [SerializeField] private Transform generationPoint;
    [SerializeField] private float distanceBetween;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var numPlatform = AleatoriPlatform();
        if (transform.position.x < generationPoint.position.x)
{
            transform.position = new Vector3(transform.position.x + distanceBetween, transform.position.y, transform.position.z);
            var instance = Instantiate(_platform[numPlatform], transform.position, Quaternion.identity);
            instance.transform.parent = transform.parent;
        }
    }

    public int AleatoriPlatform()
    {
        var platform = Random.Range(0, _platform.Length);
        return platform;
    }
}
