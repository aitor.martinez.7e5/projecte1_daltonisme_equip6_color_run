using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Controller : MonoBehaviour, IDamageble
{
    public int damagePlayer;
    public float fuerzaSalto;
    public float speed;
    private float move;
    private Rigidbody2D rb;
    private Animator anim;
    private int _doblesalto = 0;
    private bool canJump;
    private float _positionY;
    private bool _isDeath = false;
   

    public int damage {get;set;}
    public GameObject results;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        canJump = true;
        _positionY = transform.position.y-7f;
       


    }

    // Update is called once per frame
    void Update()
    {
        if (!_isDeath)
        {
            Move();
            Jump();
            Falling();
        }
 
    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && canJump )
        {
            _doblesalto++;
            anim.SetBool("isJumping", true);
            anim.SetBool("isRunning", false);
            rb.velocity = new Vector2(0, 0);
            rb.AddForce(new Vector2(0, fuerzaSalto));
            
        }
        

        if (_doblesalto == 2)
        {
            canJump = false;
        }
    }

    private void Move()
    {
        if (Time.timeScale == 1f)
        {
            transform.position = new Vector3(Time.deltaTime * 7 + transform.position.x, transform.position.y, 0);
            GameManager.Instance.CountDistance();
        }
    }
    void Falling()
    {
        if (rb.velocity.y <= -0.25f)
        {
            anim.SetBool("isJumping", false);
            anim.SetBool("isFalling", true);
        }
        if (transform.position.y <= _positionY)
        {
            //gameObject.GetComponent<SpriteRenderer>().enabled = false;
            ShowResults();
        }

    }

   


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            
            canJump = true;
            anim.SetBool("isJumping",false);
            anim.SetBool("isFalling", false);
            anim.SetBool("isRunning", true);
            _doblesalto = 0;
            
        }

        /*if (collision.gameObject.tag == "Enemy")
        {
            anim.SetBool("isDeath", true);
            Death();

        }*/

        if(collision.gameObject.tag == "Trap")
        {
            _isDeath = true;
            rb.gravityScale = 0;
            anim.SetBool("isDeath", true);
            
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Coin"))
        {
            Destroy(collision.gameObject);
            GameManager.Instance.CountCoins();
        }


        if (collision.gameObject.tag == "Enemy")
        {
            _isDeath = true;
            rb.gravityScale = 0;
            anim.SetBool("isDeath", true);

        }
    }
   

    public void AplyDamage(int damage)
    {
        _isDeath = true;
        Destroy(gameObject.GetComponent<Collider2D>());
        anim.SetBool("isDeath", true);
        
    }
    public void ShowResults()
    {
        results.SetActive(true);
        Time.timeScale = 0f;
    }
}
