using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Orb_Controller : MonoBehaviour
{
    [SerializeField]
    GameObject laser;
    [SerializeField]
    private float bulletForce;
    [SerializeField]
    private Transform target;
    private Camera _camera;
    private bool canShoot;
    public float attack_speed;
    public float attack_size;
   
    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;  
        canShoot = true;
    }
    // Update is called once per frame
    void Update()
    {
        Orb_Control();
        Shoot();
    }
    void Orb_Control()
    {
        Vector3 mouseWorldPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPosition.z = 0;
        //mouseWorldPosition.y = target.position.y;
        //Debug.Log(mouseWorldPosition);     
       target.position = new Vector3(target.position.x,mouseWorldPosition.y,target.position.z);
    }
    void Shoot()
    {
        if (canShoot && Time.timeScale == 1f)
        {
            if (Input.GetMouseButtonDown(0))
            {
                var shoot = Instantiate(laser, transform.position, Quaternion.identity);
                shoot.transform.localScale *= attack_size;
                shoot.transform.position = target.position;
                shoot.GetComponent<Rigidbody2D>().velocity = target.right * bulletForce;
                //shoot.transform.up = Vector2.down;
                StartCoroutine(ShootCooldown());
            }
        }
    }
    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attack_speed);
        canShoot = true;
    }

}
