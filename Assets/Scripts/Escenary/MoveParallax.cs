using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveParallax : MonoBehaviour
{
    public float Speed;
    void Update()
    {
        transform.position += new Vector3(Speed * 1, 0, 0);
    }
}
