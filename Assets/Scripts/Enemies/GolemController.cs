using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemController : EnemyController
{
    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
        CheckHealth();
        GetDistanceToPlayer();
        if (!isDeathEnemy) Movement();

    }
    void Movement()
    {
        if (distance < distanceToFollowPlayer)
        {
            transform.position = Vector2.MoveTowards(this.transform.position, _objectToHitOrFollow.transform.position, enemyData.speed * Time.deltaTime);
        }
    }
    public override void AplyDamage(int damageTaken)
    {
        base.AplyDamage(damageTaken);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                damageble.AplyDamage(enemyData.damage);
            }
        }

    }
}
