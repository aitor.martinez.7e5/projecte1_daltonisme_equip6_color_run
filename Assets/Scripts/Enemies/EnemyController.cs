using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Parent class of all enemies
/// </summary>
public class EnemyController : MonoBehaviour, IDamageble
{
    public EnemyData enemyData;
    protected int currentHeatlh;
    protected GameObject _objectToHitOrFollow;
    protected Rigidbody2D _rB;
    protected float distance;
    protected Vector2 direction;
    protected Animator animator;
    public float distanceToFollowPlayer;
    protected bool isDeathEnemy;
    public int damage { get=> enemyData.damage; set => enemyData.damage = value; }

    public virtual void Start()
    {
        _objectToHitOrFollow = GameObject.Find("Player");
        currentHeatlh = enemyData.health;
        animator = GetComponent<Animator>();
        isDeathEnemy = false;
    }
    public virtual void Update()
    {
        animator.SetBool("isDeath", isDeathEnemy);
    }
    protected virtual void CheckHealth()
    {
        if (currentHeatlh <= 0)
        {
            isDeathEnemy = true;
            Destroy(this.gameObject.GetComponent<Collider2D>());
        }
    }
    protected void GetDistanceToPlayer()
    {
        distance = Vector2.Distance(transform.position, _objectToHitOrFollow.transform.position);
        direction = _objectToHitOrFollow.transform.position - transform.position;
        direction.Normalize();
    }
    public virtual void AplyDamage(int damage)
    {
        animator.SetBool("isTakingHit",true);
        currentHeatlh -= damage;
    }
    public void isDeath()
    {
        Destroy(this.gameObject);
    }
}
