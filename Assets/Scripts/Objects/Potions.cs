using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Potions : MonoBehaviour
{
    public delegate  void TouchPotion();
    public static event TouchPotion OnTouchPotion;
    [SerializeField]
    public Sprite[] potions;

    /*
     0 = verde
     1 = Roja
     2 = azul
     */

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //var potis = GameObject.Find("Boss").GetComponent<BossController>().Colorblind();
                OnTouchPotion();
            Destroy(gameObject);
            //collision.gameObject.GetComponent<SpriteRenderer>

        }
    }


}

