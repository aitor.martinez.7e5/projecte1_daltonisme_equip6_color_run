using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class contains all about to what can collide the laser and the damage that it will dealt
/// </summary>
public class LaserController : MonoBehaviour , IDamageble
{
    public int damage;
    private void Start()
    {
       Destroy(gameObject, 5f);
    }
    int IDamageble.damage { get => damage; set => damage = value; }

    public void AplyDamage(int damage)
    {        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            Destroy(this.gameObject);
        }
        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null)
        {
            if (collision.gameObject.CompareTag("Enemy"))
            {
                damageble.AplyDamage(damage);
                Destroy(this.gameObject);
            }
        }
    }
}
