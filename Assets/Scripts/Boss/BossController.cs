using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;
using Wilberforce;
using Color = UnityEngine.Color;

public class BossController : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;
    [SerializeField]
    private GameObject BossBarrier;
    public GameObject[] enemies;
    [SerializeField] private GameObject fireball;
    private float fireballForce = 1f;
    public float spawn_time = 4f;
   
    private float distance = 4f;
    RaycastHit2D hit;
    private bool canSpawn;
    private bool _OverGround = false;
    [SerializeField] private GameObject potions;
    [SerializeField] private Colorblind colorblind;
    private int color;
    private float _timer = 0;
    private float _timerattack = 0;
    private float _timerbossfight = 0;
    public float hp = 900;
    public bool bossfight;

    private void Awake()
    {
        bossfight = false;
        color = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = player.GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        colorblind.Type = 0;
        sr.flipX = true;
        canSpawn = true;


    }
    private void OnEnable()
    {
        Potions.OnTouchPotion += Colorblind;
    }
    private void OnDisable()
    {
        Potions.OnTouchPotion -= Colorblind;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("bossfight =" + bossfight);
        Move();
        OverGround();
        if(!bossfight) _BossBarrier();
        
        if(hp>0)SpawnAnimation();
        if (hp <= 0)
        {
            Dead(); 
        }
        if(_timerbossfight <= 180)
        {
            
            SpawnPotions();
            _timer += Time.deltaTime;
        }

        _timerbossfight += Time.deltaTime;
           



    }


    private void Move()
    {
        transform.position = new Vector3(player.transform.position.x + 7.61f, player.transform.position.y, transform.position.z);
    }


    void OverGround()
    {
        hit = Physics2D.Raycast(this.transform.position, Vector2.down, distance);
        Debug.DrawRay(this.transform.position, Vector2.down * distance, Color.blue);
        try
        {
            if (!hit.collider.CompareTag("Ground"))
            {
                _OverGround=true;
            }

        }
        catch {}

        
    } 
    
    private void SpawnAnimation()
    {
        if (_OverGround && canSpawn && bossfight==false)
        {
            animator.SetBool("isSpawning", true);

        }else if(bossfight&canSpawn)
        {
            Debug.Log("buenos dias");
            animator.SetBool("isSpawning", true);
        }
    }

    void _BossBarrier()
    {
        try
        {
            BossBarrier.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            if (_timerbossfight >= 180)
            {
                var anim = BossBarrier.GetComponent<Animator>();
                anim.SetBool("Explode", true);
            }
        }

        catch
        {

        }
    }

    
    void StopSpawning()
    {
        if (!bossfight)
        {
            animator.SetBool("isSpawning", false);
            var instance2 = Instantiate(enemies[Random.Range(0, enemies.Length)], new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z), Quaternion.identity);
            _OverGround = false;
            StartCoroutine(SpawnCooldown());
        }else
        {
            animator.SetBool("isSpawning", false);


            fireball.GetComponent<Rigidbody2D>().velocity = transform.right * fireballForce;
      
            Vector3 distance =  player.transform.position - transform.position;
            var shoot = Instantiate(fireball, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.Euler(0, 0, -90f));// new Quaternion(0f, 0f, -90f, 0f)); ;
            shoot.GetComponent<Rigidbody2D>().AddForce(distance, ForceMode2D.Impulse);






            StartCoroutine(SpawnCooldown());
        }
        
    }
    private IEnumerator SpawnCooldown()
    {
        canSpawn = false;
        yield return new WaitForSeconds(spawn_time);
        canSpawn = true;
    }

    void SpawnPotions()
    {
        if (_OverGround && _timer > 20)
        {
            _timer = 0;
            var pot = Instantiate(potions, transform.position, Quaternion.identity);
            pot.GetComponent<SpriteRenderer>().sprite = pot.GetComponent<Potions>().potions[color];
        }
    }


    private void Dead()
    {
        Debug.Log("hola123r4");
        animator.SetBool("isDead", true);

    }


    private void Destroy()
    {
        Debug.Log("hola123r4");
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Laser")
        {
            hp -= 10;
            Destroy(collision.gameObject);
            
            
        }
    }
    
    public void Colorblind()
    {
        color++;
        if (color > 3) color = 0;
        colorblind.Type = color;
        //TrapVision();
    }

   /* public void TrapVision()
    {
        switch (color)
        {
            case 0:
                _spike.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                break;
            case 1:
                _punkySpike.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                break;
            case 2:
                _punkySpike.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                _fire.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                break;
            case 3:
                _fire.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                _spike.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                break;
        }
            
        }*/
  
    }


