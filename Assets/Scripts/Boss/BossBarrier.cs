using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBarrier : MonoBehaviour
{

    public BossController Bc;
    public UIInGameManager UIInG;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DestroyBarrier()
    {
        Destroy(gameObject);
        Bc.bossfight= true;
        UIInG.Hp.SetActive(true);
        Bc.spawn_time = 2.5f;
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
            if (collision.gameObject.tag == "Laser")
            {

            Destroy(collision.gameObject);    
            }
        
    }


}
