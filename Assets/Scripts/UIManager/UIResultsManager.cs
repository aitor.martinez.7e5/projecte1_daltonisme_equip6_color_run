using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResultsManager : MonoBehaviour
{
    public Text coins;
    public Text score;
    public Text maxCurrentScore;
    public ManagerData managerData;
    public GameObject inGamePannel;
    public GameObject newRecordText;

    private void Start()
    {
        inGamePannel.SetActive(false);
        SetTextCoins();
        SetTextScore();
        SetTextMaxCurrentScore();
    }

    public void SetTextCoins()
    {
        coins.text = GameManager.Instance.TotalCoins().ToString(); 
    }
    public void SetTextScore()
    {
        score.text = "Score: " + GameManager.Instance.Metros.ToString();
    }
    public void SetTextMaxCurrentScore()
    {
        checkMaxScore();
        maxCurrentScore.text = "Max Score: "+ managerData.maxDistance.ToString();
    }
    public void LoadScene()
    {
        GameManager.Instance.ResetGame();
    }
    void checkMaxScore()
    {
        if (managerData.Distance > managerData.maxDistance)
        {
            managerData.maxDistance = managerData.Distance;
            newRecordText.SetActive(true);
        }
    }
}
