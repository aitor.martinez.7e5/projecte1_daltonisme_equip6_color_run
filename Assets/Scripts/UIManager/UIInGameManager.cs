using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInGameManager : MonoBehaviour
{
    [SerializeField] private Text coins;
    public Text Currentscore;

    public GameObject Hp;
    public Slider slider;
    public BossController Bc;
    // Start is called before the first frame update
    void Start()
    {
        Hp.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        BossHP();
        SetCoinsTextInGame();
        SetScoreTextInGame();
    }
    void SetCoinsTextInGame()
    {
        coins.text = GameManager.Instance.TotalCoins().ToString();
    }
    void SetScoreTextInGame()
    {
        Currentscore.text = GameManager.Instance.Metros.ToString();
    }

    private void BossHP()
    {
        
        slider.value = Bc.hp;
        if (Bc.hp <= 0) Hp.SetActive(false);
    }
}
