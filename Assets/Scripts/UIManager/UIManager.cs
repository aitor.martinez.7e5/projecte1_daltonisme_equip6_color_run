using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject game;
    [SerializeField]
    private GameObject menu_panel;
    [SerializeField]
    private GameObject shop_panel;
    
    [SerializeField]
    private Text coins;
    [SerializeField]
    private Text scoreboard;


    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        shop_panel.SetActive(false);
        SetCoinsText();
        SetMaxScoreboardText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPanelOff()
    {

        this.gameObject.SetActive(false);
        game.SetActive(true);
        Time.timeScale = 1;
    }

    public void OpenShop()
    {
        menu_panel.SetActive(false);
        shop_panel.SetActive(true);
    }

    public void CloseShop()
    {
        shop_panel.SetActive(false);
        menu_panel.SetActive(true);
    }
    void SetCoinsText()
    {
        coins.text = GameManager.Instance.TotalCoins().ToString();
    }
    void SetMaxScoreboardText()
    {
        scoreboard.text = "Max Score: " + GameManager.Instance.CountDistance().ToString() + "m";
    }

    void ResetScene()
    {
        SceneManager.LoadScene("MapaScene");
    }
}
