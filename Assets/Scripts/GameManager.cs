using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ManagerData _contollerData;
    public int Coins = 0;
    public int Metros = 0;
    public int _maxDistance = 0;
    private static GameManager _instance;
    private float _timer = 0f;

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }


        Coins = _contollerData.Coins;
        Metros = 0;
    }

    public int CountDistance()
    {
        Metros++;
        if (_contollerData.Distance < Metros)
        {
            _contollerData.Distance = Metros;
        }
        return _contollerData.Distance;
    }

    public void CountCoins()
    {
        Coins++;
        _contollerData.Coins = Coins;
    }

    public int TotalCoins()
    {
        return Coins;
    }

    public void ResetGame()
    {
        Metros = 0;
        SceneManager.LoadScene("MapaScene");
    }
    public void StealCoins(int coinsSteal)
    {
        Coins -= coinsSteal;
        _contollerData.Coins = Coins;
    }
    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}
