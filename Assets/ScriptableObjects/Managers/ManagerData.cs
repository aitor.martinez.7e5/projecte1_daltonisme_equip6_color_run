using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ManagerData", menuName = "ScriptableObjects/ManagerData", order = 2)]
public class ManagerData : ScriptableObject
{
    [SerializeField] private int _coins;
    [SerializeField] private int _distance;
    public int maxDistance;

    public int Coins { get { return _coins; } set { _coins = value; } }
    public int Distance { get { return _distance; } set { _distance = value; } }
}
